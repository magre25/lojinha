package edu.ifsp.lojinha.persistencia;

import java.util.List;

import edu.ifsp.lojinha.modelo.Cliente;

public class ClienteDAO {
	public Cliente save(Cliente cliente) {
		System.out.println("ClienteDAO.save()");
		
		// inserir no banco de  dados
		return cliente;
	}
	
	public List<Cliente> listAll() {
		List<Cliente> lista = List.of(
				new Cliente("Adriana Alves", "aa@dominio.com"),
				new Cliente("Bernardo Bento", "bb@dominio.com"),
				new Cliente("Carlos Cardoso", "cc@dominio.com"),
				new Cliente("Décio Dias", "dd@dominio.com")
				);
		return lista;
	}
}
