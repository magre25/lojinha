package edu.ifsp.lojinha.web;

import javax.servlet.annotation.WebListener;
import javax.servlet.http.HttpSessionEvent;
import javax.servlet.http.HttpSessionListener;

import edu.ifsp.lojinha.modelo.Usuario;

/**
 * Application Lifecycle Listener implementation class SessionListener
 *
 */
@WebListener
public class SessionListener implements HttpSessionListener {
 
    @Override // quando a sess�o foi destruida
    public void sessionDestroyed(HttpSessionEvent se)  { 
       Usuario usuario = (Usuario)se.getSession().getAttribute("usuario");
       System.out.println("Fim da sess�o. Usu�rio: " + usuario.getNome());
    }
	
}

// especificar o tempo da sess�o
// web.xml(Deployment Descriptor) ->  esse arquivo era necessario para fazer a liga��o entre um servlet e a url
// a dura��o da sess�o ainda feita por esse arquivo