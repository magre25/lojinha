package edu.ifsp.lojinha.web;

import java.io.IOException;

import javax.servlet.DispatcherType;
import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
 

/**
 * Esse filtro vai interceptar todos os request em que a url termina com .sec
 * Nesse exemplo o usuario tem que estar logado para acessar outra pagina
 */
@WebFilter(dispatcherTypes = {DispatcherType.REQUEST } 
					, urlPatterns = { "*.sec" })
public class AuthFilter implements Filter {

	public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain) throws IOException, ServletException {
		
		// ServletRequest � mais generico
		// � necessario o tipo HttpServlet para pegar a Sess�o
		// Pois isso � necessario fazer um cast para o tipo HttpServlet
		final HttpServletRequest httpRequest = (HttpServletRequest) request; // variavel do tipo HttpServlet 
		final HttpServletResponse httpResponse = (HttpServletResponse) response;
		
		// se usuario igual a nulo, n�o tem sess�o
		if(httpRequest.getSession().getAttribute("usuario") == null) {
			httpResponse.sendRedirect("login"); // retorna para o usuario fazer o login
		}else {
			// se o usuario estiver logado o processamento continua chamando o metodo chan.doFilter()
			chain.doFilter(request, response); // 
		}
	}


}
