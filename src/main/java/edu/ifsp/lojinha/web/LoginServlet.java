package edu.ifsp.lojinha.web;

import java.io.IOException;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import edu.ifsp.lojinha.modelo.Usuario;
import edu.ifsp.lojinha.persistencia.UsuarioDAO;

@WebServlet("/login")
public class LoginServlet extends HttpServlet {

	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// msotrar jsp 
		RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
		rd.forward(request, response);
	}

	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String username = request.getParameter("username");
		String password = request.getParameter("password");
		
		// verifcar usuario e senha
		UsuarioDAO dao = new UsuarioDAO();
		Usuario usuario = dao.check(username, password);
		
		if(usuario != null) {  // se o usuario existe
			
			// criando sess�o
			HttpSession session = request.getSession(); // se nao existir uma sess�o ele, caso j� exista ele mant�m
		    session.setAttribute("usuario", usuario);
		    // retornando para outra pagina
		    response.sendRedirect("index.jsp"); // reendirecionado para pagina index.jsp, finalizando a opera��o
		    //RequestDispatcher rd = request.getRequestDispatcher("index.jsp");
			//rd.forward(request, response); // forward � pra continuar uma opera��o, faz desvio no fluxo mais ainda continua a mesma opera��o
		    
		}else {
			request.setAttribute("erro", "dados inv�lidos");
			// fica na pagina sem ir pra outra
			RequestDispatcher rd = request.getRequestDispatcher("login.jsp");
			rd.forward(request, response);
		}
		
	}

}
