package edu.ifsp.lojinha.web;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;


@WebServlet("/logout")
public class LogoutServlet extends HttpServlet {
	private static final long serialVersionUID = 1L;
  
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		
		// encerrar a sess�o
		HttpSession session = (HttpSession) request.getSession();
		session.invalidate(); // destroi a sess�o -> uma que invalida a sess�o volta ao index
		
		// voltar ao index
		response.sendRedirect("index.jsp");
	}

}
