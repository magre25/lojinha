<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Lojinha do Zé - Login</title>
<link rel="stylesheet" href="css/login.css">

</head>
<body>	
	<div class="main">
		<h1>Lojinha do Zé</h1>
	
		<form action="login" method="post">
			<fieldset> <!-- borda no formulario -->
			<legend>Login</legend>
			<p>
			<label for="username">Usuário: </label>
			<input type="text" id="username" name="username" min="3" required placeholder="username">
			</p>
			
			<p>
			<label for="password">Senha: </label>
			<input type="password" id="password" name="password" required placeholder="password">
			</p>
					
			<button type="submit">Enviar</button>
			</fieldset>
		</form>
	</div>
	
<% 
	String erroMsg = (String)request.getAttribute("erro"); // recuperando atributo da servlet
	if (erroMsg != null) {
%>
	<p class="erro"><%= erroMsg %></p>
<%
	}
%>
</body>
</html>
