<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>   

<!DOCTYPE html>
<html>
<head>
<meta charset="UTF-8">
<title>Lojinha do Zé</title>
</head>

<body>
	<h1>Lojinha do Zé</h1>
	<%-- Nova versão utilizando JSTL e EL --%>
<c:choose>
	<c:when test="${not empty sessionScope.usuario}"> <%-- se usuario que esta no copo da sessao nao for vazio  --%> 
		<p>Olá, ${sessionScope.usuario.nome}!(<a href="logout">Sair</a>)</p>
	</c:when>
	
	<c:otherwise> <%-- senão usuario faz login --%> 
		<p><a href="login">Login</a>
	</c:otherwise>
	
</c:choose>
	<p>Faça <a href="cadastro.html">aqui</a> o seu cadastro de cliente!</p>
	
<c:if test="${not empty sessionScope.usuario}">
	<p><a href="listar.sec">Listar clientes</a></p>	
</c:if>	

</body>
</html>